# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BooklistItem(scrapy.Item):
    '''定义存储图书详情页URL的Item类'''
    url = scrapy.Field()
    #pass
