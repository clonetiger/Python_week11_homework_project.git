# 将豆瓣读书网站上的所有图书信息爬取下来，并存储到MySQL数据库中。
# 爬取信息字段要求：[ID号、书名、作者、出版社、原作名、译者、出版年、页数、定价、装帧、丛书、ISBN、评分、评论人数]
# 步骤1：爬取所有豆瓣图书标签并存储到Redis数据库
# 第十一周作业
# 班级：Python五期
# 学员：李子坚
# get_book_tag.py

from requests.exceptions import RequestException
from pyquery import PyQuery
import requests
import json
import redis

def getPage(url):
    '''爬取指定url地址的信息，返回网页内容'''
    try:
        #定义请求头信息
        headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'}
        #执行爬取
        res = requests.get(url,headers=headers)
        #判断并返回结果
        if res.status_code == 200:
            return res.text
        else:
            return None
    except RequestException:	
        return None

def parsePage(content):
    '''解析爬取网页中的图书标签信息，并返回结果'''
    #print(content)
    #初始化，返回Pyquery对象
    doc = PyQuery(content)
    #解析网页中<table class="tagCol"><tbody><tr><td><a href="/tag/漫画">...信息（一个个图书标签信息）
    items = doc("table.tagCol > tbody > tr > td > a")
    #遍历并解析每个图书标签信息
    for item in items.items():
        yield item.attr.href    

def main():
    '''主程序函数，负责调度执行爬取处理'''
    print("开始爬取豆瓣读书网站所有图书标签信息，请稍候……")

    #豆瓣读书网站所有热门图书标签主页URL
    url = 'https://book.douban.com/tag/?view=type&icn=index-sorttags-all'
    html = getPage(url) #执行爬取
    if html:
        #指定Redis数据库信息
        rediscli = redis.StrictRedis(host='localhost', port=6379, db=0)

        #爬取图书标签逐项输出到Redis数据库
        for tag_url in parsePage(html):     #执行解析并遍历
            #逐项输出图书标签信息，存入Redis
            rediscli.lpush("book:tag_urls", tag_url)

        print("完成豆瓣读书网站所有图书标签信息爬取，共爬取图书标签%d个。"%rediscli.llen("book:tag_urls"))


#判断当前执行是否为主程序，并遍历调度主函数来爬取信息
if __name__ == '__main__':
    main()
