Python_week11_homework_project

项目介绍：
Python全栈开发第十一周作业。 

作业内容：
1）完成一个完整的《豆瓣图书网信息爬取项目》。将豆瓣读书网站上的所有图书信息爬取下来，并存储到MySQL数据库中。
爬取信息字段要求：[ID号、书名、作者、出版社、原作名、译者、出版年、页数、定价、装帧、丛书、ISBN、评分、评论人数]

班级：Python五期 

学员：李子坚


软件架构

软件架构说明


安装教程

1.运行步骤：

1）启动Redis服务；

2）打开命令行窗口，在Python_week11_homework_project文件夹下，运行命令：python get_book_tags.py，爬取所有豆瓣图书标签并存储到Redis数据库；

3）打开一个命令行窗口，进入bookslave\bookinfo文件夹，运行命令：scrapy crawl book；可以多打开几个命令行窗口，重复执行本步骤操作；

4）再打开一个命令行窗口，进入bookmaster\booklist文件夹，运行命令：scrapy crawl bookurl；

5）等待步骤4）运行结束，关闭步骤4）打开的命令行窗口；

6）等待步骤3）数据处理完成，进入等待状态，关闭步骤3）打开的命令行窗口；

7）启动MySql数据库，执行脚本文件<doubandb_books20180801.sql>中在数据库<doubandb>中创建表格<books>的SQL语句；

8）打开命令行窗口，在Python_week11_homework_project文件夹下，运行命令：python save_books.py，将Redis数据库的所有豆瓣读书网站图书信息存储到MySql数据库。


使用说明

1.get_book_tags.py			    	——Python程序文件：爬取所有豆瓣图书标签并存储到Redis数据库。

2.bookmaster						——主项目文件夹：根据Redis数据库的所有豆瓣图书标签爬取豆瓣读书网站上的图书列表页，获取所有图书详情页URL信息并存储到Redis数据库。

3.bookslave							——从项目文件夹：根据Redis数据库的所有图书详情页URL信息爬取豆瓣读书网站上的图书详情页，获取图书信息并存储到Redis数据库。

4.save_books.py						——Python程序文件：将Redis数据库的所有豆瓣读书网站图书信息存储到MySql数据库。

5.doubandb_books20180801.sql		——脚本文件，导入doubandb数据库创建books表结构和数据

6.books.txt							——豆瓣读书网站上的所有图书信息导出的文本文件

7.README.md							——项目说明文件


参与贡献
1.Fork 本项目
2.新建 Feat_xxx 分支
3.提交代码
4.新建 Pull Request


码云特技
1.使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
2.码云官方博客 blog.gitee.com 
3.你可以 https://gitee.com/explore 这个地址来了解码云上的优秀开源项目
4. GVP 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.码云官方提供的使用手册 http://git.mydoc.io/ 
6.码云封面人物是一档用来展示码云会员风采的栏目 https://gitee.com/gitee-stars/ 
