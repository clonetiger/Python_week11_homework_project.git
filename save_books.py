# 将豆瓣读书网站上的所有图书信息爬取下来，并存储到MySQL数据库中。
# 爬取信息字段要求：[ID号、书名、作者、出版社、原作名、译者、出版年、页数、定价、装帧、丛书、ISBN、评分、评论人数]
# 步骤4：将Redis数据库的所有豆瓣读书网站图书信息存储到MySql数据库
# 第十一周作业
# 班级：Python五期
# 学员：李子坚
# save_books.py

# -*- coding: utf-8 -*-

import json
import redis,json
import pymysql

def writeFile(content):
    '''将Redis数据库中的图书信息逐行输出到文件'''
    with open("./books.txt",'a',encoding="utf-8") as f:
        f.write(json.dumps(content,ensure_ascii=False)+"\n")

def main():
    '''主程序函数，从Redis数据库读取图书信息，并转存到MySql数据库'''

    # 指定Redis数据库信息
    rediscli = redis.StrictRedis(host='localhost', port=6379, db=0)
    # 指定MySql数据库信息，打开数据库连接
    mysqlcli = pymysql.connect(host="localhost",user="root",password="",db="doubandb",charset="utf8")
    #创建游标对象
    cursor = mysqlcli.cursor()

    print("正在提取Redis数据库的豆瓣读书网站图书信息，请稍候……")
    
    while True:
        if rediscli.llen("book:items") == 0:
            #选择网页解析库
            option = input("Redis数据库暂时没有图书信息，请选择退出或继续等待（q/Q—退出，其他键入—继续等待）：")
            if option in ['q','Q']:
                break
        else:
            try:
                # FIFO模式为 blpop，LIFO模式为 brpop，获取键值
                source, data = rediscli.blpop(["book:items"])

                # 从Redis list取值准备插入MySql数据库和输出到文件
                item = json.loads(data)
                book = dict(item)
                #图书信息输出到文件
                writeFile(item)
                
                #组装添加图书信息sql语句
                table = 'books'
                keys = ','.join(book.keys())
                values = ','.join(['%s']*len(book))
                sql = "insert into %s(%s) values(%s)"%(table, keys, values)
                #指定参数，并执行添加图书信息SQL语句
                cursor.execute(sql,tuple(book.values()))
                #提交事务
                mysqlcli.commit()
                
            except Exception as error_info:
                #事务回滚
                mysqlcli.rollback()
                print("SQL执行错误！原因：", error_info)

    #关闭数据库连接
    cursor.close()
    mysqlcli.close()

    print("Redis数据库的豆瓣读书网站图书信息转存到MySql数据库完成，请查看MySqlDB<doubandb>的表<books>")
    print("Redis数据库的豆瓣读书网站图书信息输出到文件完成，请查看文件<books.txt>")
    

if __name__ == '__main__':
    main()
