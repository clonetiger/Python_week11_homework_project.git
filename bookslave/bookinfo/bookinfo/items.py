# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BookItem(scrapy.Item):
    '''定义存储图书信息Item类'''
    ID = scrapy.Field()				#ID号
    title = scrapy.Field()			#书名
    author = scrapy.Field()			#作者
    press = scrapy.Field()			#出版社
    org_name = scrapy.Field()		#原作名
    translator = scrapy.Field()		#译者
    pub_date = scrapy.Field()		#出版年
    pages_num = scrapy.Field()		#页数
    price = scrapy.Field()			#定价
    binding = scrapy.Field()		#装帧
    series = scrapy.Field()			#丛书
    ISBN = scrapy.Field()			#ISBN
    score = scrapy.Field()			#评分
    comment_num = scrapy.Field()	#评论人数
